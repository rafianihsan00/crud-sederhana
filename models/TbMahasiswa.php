<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\db\ActiveRecord;

class TbMahasiswa extends ActiveRecord {
    private $name;
    private $nim;
    private $room;
    private $address;
    private $email;
    private $phone;

    public function rules() {
        return [
            [['name','nim', 'room', 'address', 'email' ,'phone'], 'required']    
        ];
    }
}