<?php

/* @var $this yii\yii\web\View */
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = 'My Yii Application';
?>
<div class="site-index">
    <h1>Input Mahasiswa</h1>

    <div class="body-content">
        <?php $form = ActiveForm::begin() ?>
    <div class="row">
        <div class="form-group">
            <div class="col-lg-6">
                <?= $form->field($mahasiswa, 'name'); ?>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="form-group">
            <div class="col-lg-6">
                <?= $form->field($mahasiswa, 'nim'); ?>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="form-group">
            <div class="col-lg-6">
                <?= $form->field($mahasiswa, 'room'); ?>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="form-group">
            <div class="col-lg-6">
                <?= $form->field($mahasiswa, 'email'); ?>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="form-group">
            <div class="col-lg-6">
                <?= $form->field($mahasiswa, 'address')->textarea(['rows' => '6']); ?>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="form-group">
            <div class="col-lg-6">
                <?= $form->field($mahasiswa, 'phone'); ?>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="form-group">
            <div class="col-lg-6">
                <div class="col-lg-3">
                    <?= Html::submitButton('Submit', ['class'=>'btn btn-primary']); ?>
                </div>
                <div class="col-log-2">
                    <a href=<?php echo yii::$app->homeUrl; ?> class="btn btn-primary">Kembali</a>
                </div>
            </div>
        </div>
    </div>
    
        <?php ActiveForm::end() ?>
    </div>
</div>