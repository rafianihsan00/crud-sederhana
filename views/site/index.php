<?php

/* @var $this yii\web\View */
use yii\helpers\Html;

$this->title = 'Presensi Absensi';
?>
<div class="site-index">

    <?php if(Yii::$app->session->hasFlash('message')): ?>
        <div class="alert alert-dismissible alert-succes">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <?php echo yii::$app->session->getFlash('message'); ?>
        </div>
    <?php endif; ?>

    <div class="jumbotron">
        <h2>Tugas LINK&MATCH</h2>
        <p class="lead">Presensi Absensi</p>
    </div>

    <div class="row">
        <span style="margin-bottom: 40px;">  <?= Html::a('Create', ['site/create'], ['class' => 'btn btn-primary']) ?> </span>

    <div class="body-content">
    <div class="table-responsive-sm">
        <table class="table">
        <caption>List of users</caption>
        <thead>
            <tr>
            <th scope="col">Action</th>
            <th scope="col">Nama</th>
            <th scope="col">Nim</th>
            <th scope="col">Kelas</th>
            <th scope="col">Alamat</th>
            <th scope="col">Email</th>
            <th scope="col">No.Telephone</th>
            <th scope="col">Action</th>
            </tr>
        </thead>
        <tbody>
            <?php if(count($mahasiswa) > 0): ?>
                <?php foreach($mahasiswa as $item): ?>
                <tr>
                        <th scope="Active">1</th>
                        <td><?php echo $item->name ?></td>
                        <td><?php echo $item->nim ?></td>
                        <td><?php echo $item->room ?></td>
                        <td><?php echo $item->address ?></td>
                        <td><?php echo $item->email ?></td>
                        <td><?php echo $item->phone ?></td>
                        <td>
                            <span><?= Html::a("Ubah", ['update', 'id' => $item->id], ['class' => 'Label Label-primary']) ?></span>
                            <span><?= Html::a("Hapus", ['delete', 'id' => $item->id], ['class' => 'Label Label-denger']) ?></span>
                        </td>
                    </tr>
                <?php endforeach; ?>
            <?php else: ?>
                <tr>
                    <td>Data tidak ditemukan</td>
                </tr>
            <?php endif; ?>
        </tbody>
        </table>
    </div>
    </div>
</div>