<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\TbMahasiswa;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $mahasiswa = TbMahasiswa::find()->all();
        return $this->render('index', ['mahasiswa'=> $mahasiswa]);
    }

    public function actionCreate()
    {
        $mahasiswa = new TbMahasiswa();
        $formData = yii::$app->request->post();
        if($mahasiswa->load($formData)){
            if($mahasiswa->save()) {
                Yii::$app->getSession()->setFlash('message', 'Data Mahasiswa Berhasil Disimpan');
                return $this->redirect(['index']);
            } else {
            Yii::$app->getSession()->setFlash('message', 'Data Mahasiswa Gagal Disimpan');
            }
        }
        return $this->render('create', ['mahasiswa' => $mahasiswa]);
    }

    public function actionUpdate($id)
    {
        $mahasiswa = TbMahasiswa::findOne($id);
        if ($mahasiswa->load(Yii::$app->request->post()) && $mahasiswa->save()) {
            Yii::$app->getSession()->getFlash('message', 'Data Mahasiswa berhasil diubah');
            return $this->redirect(['index', 'id' => $mahasiswa->id]);
        } else {
            return $this->render('update', ['mahasiswa' => $mahasiswa]);
        }
    }

    public function actionDelete($id)
    {
        $mahasiswa = TbMahasiswa::findOne($id)->delete();
        if ($mahasiswa) {
            Yii::$app->getSession()->setFlash('message', 'Berhasil hapus mahasiswa');
            return $this->redirect(['index']);
        }
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
}
